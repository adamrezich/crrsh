﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Vectrosity;

public class VectorMonitor : MonoBehaviour {
  public class Point {
    public int X;
    public int Y;
    public Point(int x, int y) {
      X = x;
      Y = y;
    }
  }

  private class VectorStroke {
    public Vector3 Begin;
    public Vector3 End;
    public bool ContinueFromPrevious = false;
    public VectorStroke(Vector3 end) {
      ContinueFromPrevious = true;
      End = end;
    }
    public VectorStroke(Vector3 begin, Vector3 end) {
      Begin = begin;
      End = end;
    }
  }

  private class VectorGlyph {
    public List<VectorStroke> Strokes = new List<VectorStroke>();
  }

  private class VectorFont {
    public string Name;
    public Dictionary<char, VectorGlyph> Glyphs = new Dictionary<char, VectorGlyph>();
    public VectorFont(string name, string path) {
      Name = name;
      TextAsset txt = (TextAsset)Resources.Load(path, typeof(TextAsset));
      string[] lines = txt.text.Split('\n');
      int lineNum = 1;
      foreach (string line in lines) {
        VectorGlyph glyph = new VectorGlyph();
        if (line.Trim() != "") {
          string[] strokes = line.Trim().Split(' ');
          foreach (string stroke in strokes) {
            string[] points = stroke.Split(';');
            if (points.Length == 1) {
              string[] numbers = points[0].Split(',');
              glyph.Strokes.Add(new VectorStroke(new Vector3(float.Parse(numbers[0]), float.Parse(numbers[1]))));
            }
            else {
              string[] beginNumbers = points[0].Split(',');
              string[] endNumbers = points[1].Split(',');
              glyph.Strokes.Add(new VectorStroke(new Vector3(float.Parse(beginNumbers[0]), float.Parse(beginNumbers[1])), new Vector3(float.Parse(endNumbers[0]), float.Parse(endNumbers[1]))));
            }
          }
        }
        Glyphs.Add((char)(lineNum), glyph);
        ++lineNum;
      }
    }
  }

  private class VectorChar {
    public char Character;
    public Color Color;
    public VectorLine LineObject;
    public Point Position;
    public bool Erasing = false;
    public bool Destroying = false;
    public bool ShouldDestroy {
      get {
        if (LineObject == null)
          return false;
        return Erasing && LineObject.color.a == 0;
      }
    }
    private VectorMonitor Monitor;
    private List<Vector3> Vertices = new List<Vector3>();
    public VectorChar(VectorMonitor monitor, char character, Color color, int x, int y) {
      Monitor = monitor;
      Character = character;
      Color = new Color(color.r, color.g, color.b, 1.0f);
      Position = new Point(x, y);

      GenerateGlyph();

      LineObject = new VectorLine("VectorChar", new List<Vector3>(), Monitor.VectorMaterial, 2f, LineType.Discrete, Joins.Weld);
    }

    public void Update() {
      var origin = Monitor.CoordinatesToWorld(Position.X, Position.Y);
      Color col = LineObject.color;
      col.a = Mathf.Max(col.a - Time.deltaTime * (Erasing ? Monitor.CharacterFadeOutSpeed : Monitor.CharacterFadeInSpeed), Erasing ? 0 : Mathf.Max(Monitor.ScreenAlpha + Monitor.Flicker, Monitor.ScreenAlpha));
      col.a *= Monitor.Alpha;
      LineObject.color = col;
      LineObject.lineWidth = Mathf.Clamp(LineObject.lineWidth + Random.Range(-0.15f, 0.15f), 1.5f, 2.5f);
      if (ShouldDestroy)
        return;
      LineObject.points3.Clear();
      float charWiggleX = Monitor.Wiggle * Random.Range(0.3f, 0.6f);
      float charWiggleY = Monitor.Wiggle * Random.Range(0.3f, 0.6f);
      foreach (Vector3 vertex in Vertices) {
        Vector3 vec = origin + new Vector3(vertex.x + charWiggleX + Monitor.Wiggle, vertex.y + charWiggleY +  Monitor.Wiggle, 0);
        LineObject.points3.Add(vec);
        Monitor.TraceLine.points3.Add(vec);
      }
      LineObject.Draw();
    }

    public void GenerateGlyph() {
      if (Monitor.Font.Glyphs.ContainsKey(Character)) {
        VectorGlyph glyph = Monitor.Font.Glyphs[Character];
        foreach (VectorStroke stroke in glyph.Strokes) {
          if (stroke.ContinueFromPrevious)
            AddSub(stroke.End.x, stroke.End.y);
          else
            AddSub(stroke.Begin.x, stroke.Begin.y, stroke.End.x, stroke.End.y);
        }
      }
      else {
        AddSub(-1, -1, 1, -1);
        AddSub(1, 1);
        AddSub(-1, 1);
        AddSub(-1, -1);
        AddSub(0, 1, 1, 0);
        AddSub(-1, 1, 1, -1);
        AddSub(-1, 0, 0, -1);
      }
    }

    private void AddSub(float x, float y) {
      var last = Vertices.Last();
      Vertices.Add(new Vector3(last.x, last.y));
      Vertices.Add(Monitor.SubGlyphCoordinates(x, y));
    }
    private void AddSub(float x1, float y1, float x2, float y2) {
      Vertices.Add(Monitor.SubGlyphCoordinates(x1, y1));
      Vertices.Add(Monitor.SubGlyphCoordinates(x2, y2));
    }

    Vector3 RandomPoint() {
      return new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height));
    }
  }

  public Material VectorMaterial;
  public VectorLine TraceLine;
  private List<VectorChar> VectorChars = new List<VectorChar>();
  private Vector3 CharacterSpacing = new Vector3(0.35f, 0.15f);
  private Vector3 CharacterDimensions = new Vector3(0.135f, 0.28f);
  private Vector3 DefaultCharacterDimensions;
  private int width = 64;
  private int height = 25;
  public int Width {
    get {
      return width;
    }
  }
  public int Height {
    get {
      return height;
    }
  }
  public Point Cursor = new Point(0, 0);
  private Color TraceColor = new Color(0.3f, 0.3f, 0.3f, 0.1f);
  private float CharacterFadeInSpeed = 3.5f;
  private float CharacterFadeOutSpeed = 1.0f;
  private float ScreenAlpha = 0.25f;
  private VectorFont Font;

  public bool Active = false;
  public float Alpha;

  private bool initialized = false;

  private bool frozen = false;

  private bool wandering = true;
  private float wanderDirection = 0;
  private float wanderSpeed = 0.04f;
  private float wanderDelta = 360f / 4f;
  private float wanderMax = 0.5f;

  private float wanderRotation = 0;
  private float wanderRotationSpeed = 0.45f;
  private float wanderRotationMax = 360f / 32f;
  private float wanderRotationDirection = 0;
  private float wanderRotationDelta = 360f / 12f;

  private float glitchAmount = 0;
  private float glitchBase = 0.005f;
  private float glitchScalar = 0.5f;
  private bool glitching = false;

  private float abrAmount = 0.5f;
  private float abrDirection = 0f;

  private float flickerAmount = 0f;
  public float Flicker {
    get {
      return Random.Range(-flickerAmount, flickerAmount);
    }
  }

  private Vector3 Origin = new Vector3(40f, 0);


  public AudioClip CursorSound;
  public AudioClip RecieveSound;
  public AudioClip KeystrokeSound;

  public GameObject ComputerObject;
  public Camera MyCamera;

  public float Wiggle {
    get {
      float amt = glitchBase + glitchScalar * glitchAmount;
      return Random.Range(-amt, amt);
    }
  }

  private Vector3 TopLeft {
    get {
      // TODO: Cache
      return transform.position - Origin + new Vector3(-width * CharacterDimensions.x / 2f - (width - 1) * CharacterSpacing.x * CharacterDimensions.x / 2f, height * CharacterDimensions.y / 2f + (height - 1) * CharacterSpacing.y * CharacterDimensions.y / 2f);
    }
  }

  public Vector3 CoordinatesToWorld(int x, int y) {
    return TopLeft + new Vector3(CharacterDimensions.x * x + CharacterSpacing.x * CharacterDimensions.x * x, -(CharacterDimensions.y * y + CharacterSpacing.y * CharacterDimensions.y * y));
  }

  public Vector3 SubGlyphCoordinates(float x, float y) {
    return new Vector3(CharacterDimensions.x / 2 + x * CharacterDimensions.x * 0.5f, CharacterDimensions.y / 2 + y * CharacterDimensions.y * 0.5f);
  }

	// Use this for initialization
	void Start() {
    DefaultCharacterDimensions = CharacterDimensions;

    // Load font
    Font = new VectorFont("default", "VectorMonitor/Fonts/0");
	}
	
	// Update is called once per frame
	void Update() {

    Alpha = Mathf.Lerp(Alpha, Active ? 1f : 0f, 0.25f);

    if (!initialized) {
      initialized = true;
      VectorLine.SetCanvasCamera(MyCamera);
      TraceLine = new VectorLine("TraceLine", new List<Vector3>(), VectorMaterial, 1f, LineType.Continuous, Joins.Weld);
    }
    TraceLine.color = new Color(TraceColor.r, TraceColor.g, TraceColor.b, TraceColor.a * Alpha);

    if (!Active)
      return;

    // Monitor flicker (GOOD)
    var vig = MyCamera.GetComponent<UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration>();
    var chrabr = MyCamera.GetComponent<ChromaticAberration>();
    vig.intensity = Mathf.Clamp(vig.intensity + Random.Range(-20f, 20f) * Time.deltaTime, 4.25f, 4.75f);


    /*float abrDeltaAmt = Mathf.PI / 12f;
    float abrBonus = 1f;
    if (abrAmount > -0.15f && abrAmount < 0.15f)
      abrBonus = 2f;
    abrDirection += Random.Range(-abrDeltaAmt, abrDeltaAmt) * abrBonus;

    abrAmount = Mathf.Sin(abrDirection) * 0.35f;
    Debug.Log(abrDirection.ToString() + " " + abrAmount.ToString());*/

    // GOOD
    abrAmount = 0.45f;
    chrabr.ChromaticAbberation = abrAmount + Mathf.Sign(abrAmount) * (glitchAmount * glitchScalar * 16f);
    float amt = Mathf.Clamp(Camera.main.backgroundColor.r + Random.Range(-0.2f, 0.2f) * Time.deltaTime, 0.03f, 0.05f);
    MyCamera.backgroundColor = new Color(amt, amt, amt);

    /*
    if (Input.GetKeyDown(KeyCode.F1)) {
      Glitch(0.075f);
    }
    if (Input.GetKeyDown(KeyCode.F2)) {
      Glitch(0.15f);
    }
    */
    if (Input.GetKey(KeyCode.F8)) {
      CharacterDimensions = DefaultCharacterDimensions;
    }
    if (Input.GetKey(KeyCode.F9)) {
      CharacterDimensions += new Vector3(-0.01f * Time.deltaTime, 0, 0);
    }
    if (Input.GetKey(KeyCode.F10)) {
      CharacterDimensions += new Vector3(0.01f * Time.deltaTime, 0, 0);
    }
    if (Input.GetKey(KeyCode.F11)) {
      CharacterDimensions += new Vector3(0, -0.01f * Time.deltaTime, 0);
    }
    if (Input.GetKey(KeyCode.F12)) {
      CharacterDimensions += new Vector3(0, 0.01f * Time.deltaTime, 0);
    }
    // TODO: Monitor glitch effects, etc.
    if (frozen) {
    }
    else {
      if (Random.Range(0, 120) == 0) {
        Glitch(Random.Range(0.015f, 0.055f));
      }

      // Wander the position
      gameObject.transform.position += new Vector3(Mathf.Cos(wanderDirection) * Random.Range(0, wanderSpeed) * Time.deltaTime, Mathf.Sin(wanderDirection) * Random.Range(0, wanderSpeed) * Time.deltaTime);
      wanderDirection += Random.Range(-wanderDelta / 2f, wanderDelta / 2f) * Time.deltaTime;

      // Wander the rotation
      float wanderRot = Mathf.Sin(wanderRotationDirection) * Random.Range(0, wanderRotationSpeed) * Time.deltaTime;
      wanderRotationDirection += Random.Range(-wanderRotationDelta / 2f, wanderRotationDelta / 2f) * Time.deltaTime;
      wanderRotation += wanderRot;
      var cam = MyCamera;
      cam.transform.eulerAngles = new Vector3(0, 0, cam.transform.eulerAngles.z + wanderRot);

      // Cap the wandering
      if (Vector3.Distance(gameObject.transform.position, Origin) > wanderMax || Mathf.Abs(wanderRotation) > wanderRotationMax) {
        Reset();
      }

      if (glitching) {
        glitchAmount = Mathf.Lerp(glitchAmount, glitchBase, 0.1f);
        if (glitchAmount <= glitchBase + 0.001f)
          glitching = false;

      }
    }
  }

  void Glitch(float amount) {
    glitching = true;
    glitchScalar = amount; // in the Zero version of this I wrote the comment "confuing but w/e" and I'm not 100% sure why
    glitchAmount = 1; // ???? (okay I'm totally confused)

  }

  void Reset() {
    // TODO: Flicker, but not if reset too recently?
    Glitch(0.2f);
    gameObject.transform.position = Origin;
    MyCamera.transform.eulerAngles = Vector3.zero;
    wanderRotation = 0;
    wanderRotationDirection = 0;
  }

  public void BeginDraw() {
    TraceLine.points3.Clear();
    if (VectorChars.Count == 0) {
      TraceLine.points3.Add(TopLeft);
      TraceLine.points3.Add(TopLeft);
    }
  }

  public void EndDraw() {
    foreach (VectorChar vc in VectorChars) {
      vc.Update();
    }
    foreach (VectorChar vc in VectorChars.Where(vch => vch.ShouldDestroy)) {
      VectorLine.Destroy(ref vc.LineObject);
      vc.LineObject = null;
      vc.Destroying = true;
    }
    VectorChars.RemoveAll(vc => vc.Destroying);
    TraceLine.lineWidth = Mathf.Clamp(TraceLine.lineWidth + Random.Range(-0.5f, 0.5f), 0.5f, 1.5f);
    TraceLine.Draw();
  }

  private void HandleOverflow() {
    int yOver = Cursor.Y - height -1;
    if (yOver > 0) {
      foreach (VectorChar vc in VectorChars.Where(vch => vch.Position.Y < yOver)) {
        vc.Erasing = true;
      }
      foreach (VectorChar vc in VectorChars.Where(vch => vch.Position.Y >= yOver)) {
        vc.Position.Y -= yOver;
      }
      Cursor.Y -= yOver;
    }
  }

  public void Add(char c) {
    if (c == '\r' || c == '\n') {
      Cursor.X = 0;
      ++Cursor.Y;
      HandleOverflow();
    }
    else {
      VectorChars.Add(new VectorChar(this, c, Color.white, Cursor.X, Cursor.Y));
      ++Cursor.X;
      while (Cursor.X >= width) {
        Cursor.X -= width;
        ++Cursor.Y;
        HandleOverflow();
      }
    }
  }
  public void Remove() {
    if (VectorChars.Count(vch => !vch.Erasing) == 0)
      return;
    VectorChar vc = VectorChars.Last(vch => !vch.Erasing);
    Cursor.X = vc.Position.X;
    Cursor.Y = vc.Position.Y;
    vc.Erasing = true;
    /*VectorLine.Destroy(ref vc.LineObject);
    VectorChars.Remove(vc);*/
    /*--Cursor.X;
    while (Cursor.X < 0) {
      Cursor.X += Width;
      --Cursor.Y;
    }*/
  }
  public void Clear() {
    // TODO: Redo
    foreach (VectorChar vc in VectorChars) {
      vc.Erasing = true;
    }
    Cursor.X = 0;
    Cursor.Y = 0;
  }

  public void PlaySound_Keystroke() {
    AudioSource source = gameObject.GetComponent<AudioSource>();
    source.pitch = Random.Range(0.6f, 1.4f);
    source.PlayOneShot(KeystrokeSound);
  }

  public void PlaySound_RecieveCharacter() {
    AudioSource source = gameObject.GetComponent<AudioSource>();
    source.pitch = Random.Range(0.6f, 1.4f);
    source.PlayOneShot(RecieveSound);
  }
}
