﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class RasterMonitor : MonoBehaviour {

  public class Point {
    public int X;
    public int Y;
    public Point(int x, int y) {
      X = x;
      Y = y;
    }
  }

  private class RasterChar {
    public Point Position;
    public char Character;
    public Color Color;
    public GameObject Object;

    public RasterChar(char c, int x, int y, GameObject obj) {
      Position = new Point(x, y);
      Character = c;
      Object = obj;
    }
  }

  public GameObject CharPrefab;

  private GameObject cursorObject = null;

  private Vector3 Origin = new Vector3(-1000f - 320f, 200f);

  private List<Sprite> fontSprites = new List<Sprite>();

  private List<RasterChar> rasterChars = new List<RasterChar>();

  private Point Cursor = new Point(0, 0);

  private int lastLineEnd = 0;

  public const int Width = 70;
  public const int Height = 25;

  private float blinkInterval = 0.125f;
  private float lastBlink = 0;

	// Use this for initialization
	void Start() {
    fontSprites = Resources.LoadAll<Sprite>("Codepage737").ToList();

    cursorObject = Instantiate(CharPrefab, Origin + new Vector3(0, 0), Quaternion.identity) as GameObject;
    var spr = cursorObject.GetComponent<SpriteRenderer>();
    spr.sprite = fontSprites[(int)'_' - 2];
    spr.color = new Color(0.75f, 0.75f, 0.75f);

    /*WriteLine("   Phoenix - AwardBIOS v6.00PC, An Energy Star Ally");
    WriteLine("   Copyright (C) 1984-2005, Phoenix Technologies LTD");
    WriteLine("");
    WriteLine("ASUS A8N-SLI Premium ACPI BIOS Revision 1011-001");
    WriteLine("");
    WriteLine("Main Processor: AMD Athlon(tm) 64 Processor 4000+");
    WriteLine("Memory Testing: 2096152K OK (Installed Memory: 2097152K)");
    WriteLine("Memory information: DDR 400  Dual Channel, 128-bit");
    WriteLine("");
    WriteLine("Chipset Model: nForce 4");
    WriteLine("Primary IDE Master   : None");
    WriteLine("Primary IDE Slave    : None");
    WriteLine("Secondary IDE Master : None");
    WriteLine("Secondary IDE Slave  : None");
    WriteLine("\n\n\n\n\n\n\n\n\n");
    WriteLine("Press F1 to continue, DEL to enter SETUP");
    WriteLine("12/07/2005-NF-CK804-A8NSLI-P-00");*/
    StartCoroutine("Boot");
	}

  IEnumerator Boot() {
    // 70 chars across

    MoveTo(0, 24);
    WriteLine("Press F1 to continue, DEL to enter SETUP");
    WriteLine("12/07/2005-NF-CK804-A8NSLI-P-00");

    MoveTo(0, 0);
    WriteLine("   Phoenix - AwardBIOS v6.00PC, An Energy Star Ally");
    WriteLine("   Copyright (C) 1984-2005, Phoenix Technologies LTD");
    WriteLine("");
    WriteLine("ASUS A8N-SLI Premium ACPI BIOS Revision 1011-001");
    yield return new WaitForSeconds(1f);

    WriteLine("");
    WriteLine("Main Processor: AMD Athlon(tm) 64 Processor 4000+");
    yield return new WaitForSeconds(0.5f);

    int memtestLine = Cursor.Y;
    int memtestChecked = 0;
    int memtestTotal = 1024 * 8;
    while (memtestChecked < memtestTotal) {
      ChangeLine(memtestLine, string.Format("Memory Testing: {0}K OK", memtestChecked));
      memtestChecked = Mathf.Min(memtestChecked + Random.Range(4, 64), memtestTotal);
      yield return new WaitForEndOfFrame();
    }
    ChangeLine(memtestLine, string.Format("Memory Testing: {0}K OK (Installed Memory: {1}K)", memtestChecked, memtestTotal));
    yield return new WaitForSeconds(1f);

    WriteLine("");
    WriteLine("Chipset Model: nForce 4");

    int ideLine = Cursor.Y;
    WriteLine("Detecting IDE drives ...");
    yield return new WaitForSeconds(1f);

    ChangeLine(ideLine, "Primary IDE Master   : None");
    WriteLine("Primary IDE Slave    : None");
    WriteLine("Secondary IDE Master : None");
    WriteLine("Secondary IDE Slave  : None");
    yield return new WaitForSeconds(4f);

    Clear();
    yield return new WaitForSeconds(1.5f);

    WriteLine((char)186 + " CPU Type          : PENTIUM III        Base Memory      : 640K     " + (char)186);
    WriteLine((char)186 + " Co-Processor      : Installed          Extended Memory  : 8192K    " + (char)186);
    WriteLine((char)186 + " CPU Clock         : 480MHz             Cache Memory     : 512K     " + (char)186);
    WriteLine((char)199 + new string((char)196, 68) + (char)182);
    WriteLine((char)186 + " Diskette Drive  A : 1.44M, 3.5in.      Display Type     : VDP/VGA  " + (char)186);
    WriteLine((char)186 + " Diskette Drive  B : None               Serial Port(s)   : 3F8      " + (char)186);
    WriteLine((char)186 + " Pri. Master  Disk : None               Parallel Port(s) : 378      " + (char)186);
    WriteLine((char)186 + " Pri. Slave   Disk : None               SDRAM at Row(s)  : 2  3     " + (char)186);
    WriteLine((char)186 + " Sec. Master  Disk : None                                           " + (char)186);
    WriteLine((char)186 + " Sec. Slave   Disk : None               Power Management : Enabled  " + (char)186);
    WriteLine((char)200 + new string((char)205, 68) + (char)188);
    WriteLine("");
    WriteLine("PCI device listing ...");
    //         012345678 012345678 012345678 012345678 012345678 012345678 0123456789
    WriteLine("Bus No. Dev No. Func No. Vendor ID Dev ID Dev Class              IRQ");
    WriteLine(new string((char)196, 70));
    WriteLine("  0       7        1        B8B6    7111  IDE Controller          14");
    WriteLine("  0       7        2        B8B6    7112  Serial Bus Controller   10");
    WriteLine("  0       9        0        1315    8881  Multimedia Device        5");
    WriteLine("  0       9        1        1319    8882  Input Device            NA");
    WriteLine("  0      11        0        12B9    1886  Simple COMM. Controller 10");
    WriteLine("  1       0        0        5333    BA22  Display Controller      11");
    WriteLine("                                          ACPI Controller          9");
    WriteLine("");
    WriteLine("");
    WriteLine("Verifying DMI Pool Data ..........");
    yield return new WaitForSeconds(4f);

    WriteLine("EZ-BIOS: Initializing...");
    WriteLine("EZ-BIOS: Press ENTER to boot from floppy...");
    // WriteLine("  Loading crrsh bootstrapper...");


    yield break;
  }

  void MoveTo(int x, int y) {
    Cursor.X = x;
    Cursor.Y = y;
    lastLineEnd = x;
  }

  void ChangeLine(int y, string s) {
    var toRemove = rasterChars.Where(rc => rc.Position.Y == y);
    foreach (var rchar in toRemove) {
      Destroy(rchar.Object);
    }
    rasterChars.RemoveAll(rc => rc.Position.Y == y);
    Cursor.X = 0;
    Cursor.Y = y;
    WriteLine(s);
  }

  void WriteLine(string s) {
    if (s == "") {
      lastLineEnd = 0;
      Cursor.X = 0;
      ++Cursor.Y;
      return;
    }
    if (Cursor.Y >= Height) {
      int ydiff = Cursor.Y - Height;
      foreach (var rchar in rasterChars) {
        rchar.Position.Y -= ydiff;
        if (rchar.Position.Y < 0)
          Destroy(rchar.Object);
        else
          rchar.Object.transform.position = Origin + new Vector3(rchar.Position.X * 9f, -rchar.Position.Y * 16f);
      }
      rasterChars.RemoveAll(rc => rc.Position.Y < 0);
      Cursor.Y -= ydiff;
    }
    Write(0, Cursor.Y, s);
    lastLineEnd = Cursor.X;
    ++Cursor.Y;
    Cursor.X = 0;
  }

  void Write(int x, int y, string s) {
    Cursor.X = x;
    Cursor.Y = y;
    foreach (char c in s) {
      if (c == '\n' || c == '\r') {
        Cursor.X = 0;
        ++Cursor.Y;
      }
      else {
        Put(c, Cursor.X, Cursor.Y);
        ++Cursor.X;
      }
    }
  }

  void Put(char c, int x, int y) {
    if (c == ' ')
      return;
    GameObject obj = Instantiate(CharPrefab, Origin + new Vector3(x * 9f, -y * 16f), Quaternion.identity) as GameObject;
    var spr = obj.GetComponent<SpriteRenderer>();
    spr.sprite = fontSprites[(int)c - 2];
    spr.color = new Color(0.75f, 0.75f, 0.75f);
    var toRemove = rasterChars.Where(rc => rc.Position.X == x && rc.Position.Y == y);
    foreach (var rchar in toRemove) {
      Destroy(rchar.Object);
    }
    rasterChars.RemoveAll(rc => rc.Position.X == x && rc.Position.Y == y);
    rasterChars.Add(new RasterChar(c, x, y, obj));
  }

  void Clear() {
    foreach (var rchar in rasterChars)
      Destroy(rchar.Object);
    rasterChars.Clear();
    MoveTo(0, 0);
  }
	
	// Update is called once per frame
	void Update() {
    cursorObject.transform.position = Origin + new Vector3(lastLineEnd * 9f, -(Cursor.Y == 0 ? Cursor.Y : Cursor.Y - 1) * 16f);
    if (Time.realtimeSinceStartup > lastBlink + blinkInterval) {
      cursorObject.GetComponent<SpriteRenderer>().enabled = !cursorObject.GetComponent<SpriteRenderer>().enabled;
      lastBlink += blinkInterval;
    }
	}
}
