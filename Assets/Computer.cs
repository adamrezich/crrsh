﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Computer : MonoBehaviour {

  public enum DisplayMode {
    Off,
    Raster,
    Vector
  }

  public DisplayMode CurrentDisplayMode = DisplayMode.Raster;

  public GameObject VectorMonitor;
  public GameObject RasterImage;

	// Use this for initialization
	void Start() {
	
	}
	
	// Update is called once per frame
	void Update() {
    RawImage rasterImage = RasterImage.GetComponent<RawImage>();
    VectorMonitor vectorMonitor = VectorMonitor.GetComponent<VectorMonitor>();
	  if (Input.GetKeyDown(KeyCode.F1)) {
      // Raster
      vectorMonitor.Active = false;
    }
    if (Input.GetKeyDown(KeyCode.F2)) {
      // Vector
      vectorMonitor.Active = true;
    }
    if (Input.GetKeyDown(KeyCode.F3)) {
      // Off
    }
    rasterImage.color = new Color(1f, 1f, 1f, Mathf.Lerp(rasterImage.color.a, vectorMonitor.Active ? 0f : 1f, 0.2f));

    var bloom = Camera.main.GetComponent<UnityStandardAssets.ImageEffects.Bloom>();
    bloom.bloomThreshold = Mathf.Lerp(bloom.bloomThreshold, vectorMonitor.Active ? 0.2f : 0.5f, 0.2f);

    var noise = Camera.main.GetComponent<UnityStandardAssets.ImageEffects.NoiseAndGrain>();
    noise.intensityMultiplier = Mathf.Lerp(noise.intensityMultiplier, vectorMonitor.Active ? 0.25f : 0.15f, 0.2f);

    if (!vectorMonitor.Active) {
      var chrabr = Camera.main.GetComponent<ChromaticAberration>();
      chrabr.ChromaticAbberation = Mathf.Lerp(chrabr.ChromaticAbberation, 0.25f, 0.2f);

      var vig = Camera.main.GetComponent<UnityStandardAssets.ImageEffects.VignetteAndChromaticAberration>();
      vig.intensity = Mathf.Lerp(vig.intensity, 1f, 0.2f);

      Camera.main.backgroundColor = Color.Lerp(Camera.main.backgroundColor, new Color(0.085f, 0.085f, 0.085f), 0.2f);
    }
	}
}
