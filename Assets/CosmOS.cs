﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CosmOS : MonoBehaviour {
  public class User {
    public string Name = "";
    public string Password = "";
    
    public User(string name, string password) {
      Name = name;
      Password = password;
    }
  }
  
  #region File System
  public class File {
    public string Name;
    public Directory Parent = null;
    public File(string name) {
      Name = name;
    }
    public string Path {
      get {
        Directory dir = Parent;
        if (dir == null)
          return "/";
        string path = "";
        while (dir != null) {
          path = dir.Name + "/" + path;
          dir = dir.Parent;
        }
        path += Name + "/";
        return path;
      }
    }
  }
  public class Directory : File {
    public List<File> Contents = new List<File>();
    public Directory(string name) : base(name) {}
    public Directory this[string key] {
      get {
        Directory f = Contents.Where(x => x.Name == key).FirstOrDefault() as Directory;
        if (f != null)
          return f;
        return null;
      }
    }
    public File Add(File file) {
      file.Parent = this;
      Contents.Add(file);
      Contents = Contents.OrderBy(o => o.Name).ToList();
      return file;
    }
  }
  public class TextFile : File {
    public string Contents = "";
    public TextFile(string name, string contents) : base(name) {
      Contents = contents;
    }
  }
  public class Binary : File {
    public delegate IEnumerator Subroutine(List<string> argv);
    public Subroutine Contents;

    
    // Help system
    public string Description;
    public string AdditionalInfo;
    public Argument[] Arguments;
    
    
    public Binary(string name, Argument[] arguments, Subroutine main) : base(name) {
      Name = name;
      Arguments = arguments;
      Contents = main;
    }
    public Binary(string name, string description, Argument[] arguments, Subroutine main) : base(name) {
      Description = description;
      Arguments = arguments;
      Contents = main;
    }
    public Binary(string name, string description, Argument[] arguments, string additionalInfo, Subroutine main) : base(name) {
      Description = description;
      Arguments = arguments;
      AdditionalInfo = additionalInfo;
      Contents = main;
    }
    
    public string HelpContents {
      get {
        return Description + "\n\n " + Name;
      }
    }
    public bool ArgumentsAreValid(List<string> argv) {
      if (Arguments == null)
        return argv.Count - 1 == 0;
      
      int minimum = 0;
      int optional = 0;
      bool variadic = false;
      foreach (Argument arg in Arguments) {
        if (arg.Required)
          ++minimum;
        else
          ++optional;
        if (arg.Variadic)
          variadic = true;
      }
      return (argv.Count - 1 >= minimum && (variadic || argv.Count - 1 <= minimum + optional));
    }
  }
  public struct Argument {
    public string Name;
    public string Description;
    public bool Required;
    public bool Variadic;
    
    public Argument(string name, string description, bool required = true, bool variadic = false) {
      Name = name;
      Description = description;
      Required = required;
      Variadic = variadic;
    }
  }
  #endregion
  
  public enum Keys {
    Backspace = 8,
    Tab = 9,
    Enter = 13,
    Escape = 27,
    Space = 32,
  }
  public enum DisplayMode {
    Monochrome,
    SixteenColor
  }
  public enum InputMode {
    Normal,
    Suppressed,
    Hidden,
    Password
  }
  
  public User CurrentUser = null;
  public List<User> Users = new List<User>() {
    new User("root", "letmein"),
    new User("repair", "asdf")
  };
  public string InputBuffer = "";
  public string OutputBuffer = "";
  public Directory FileSystem = new Directory("");
  public Directory CurrentDirectory;
  public DisplayMode CurrentDisplayMode = DisplayMode.SixteenColor;
  private bool isRunning = false;
  private bool isRebooting = false;
  private float outputDelay = 0; // faster than 1/60 atm
  private float outputReadyTime = 0;
  private bool programRunning = false;
  private bool programRunningLast = false;
  private InputMode CurrentInputMode = InputMode.Normal;

  public GameObject MonitorObject;
  private VectorMonitor monitor;

  void Initialize() {
    WriteLine("crrsh v1.0.3.8570\n");
    WritePrompt();
  }

  void Start() {
    monitor = MonitorObject.GetComponent<VectorMonitor>();
    SetupDefaultFileSystem();
    CurrentDirectory = FileSystem;
    CurrentUser = Users[0];
  }

  void Update() {
    monitor.BeginDraw();

    if (!isRunning) {
      Initialize();
      isRunning = true;
    }

    bool beganExecuting = false;

    if (OutputBuffer.Length > 0) {
      if (OutputBuffer.Length > 0) {
        if (Time.time >= outputReadyTime) {
          if (OutputBuffer[0] != ' ' && OutputBuffer[0] != '\n' && OutputBuffer[0] != '\r') {
            outputReadyTime = Time.time + outputDelay;
            monitor.PlaySound_RecieveCharacter();
          }
          monitor.Add(OutputBuffer[0]);
          OutputBuffer = OutputBuffer.Substring(1);
        }
      }
    }
    if (CurrentInputMode != InputMode.Suppressed && OutputBuffer.Length == 0) {
      foreach (char c in Input.inputString) {
        //gameObject.GetComponent<AudioSource>().PlayOneShot(KeystrokeSound);
        monitor.PlaySound_Keystroke();
        switch (c) {
        case '\b':
          if (InputBuffer.Length > 0) {
            monitor.Remove();
            InputBuffer = InputBuffer.Substring(0, InputBuffer.Length - 1);
          }
          break;
        case '\r':
          break;
        case '\n':
          break;
        default:
          InputBuffer += c;
          if (CurrentInputMode == InputMode.Normal)
            monitor.Add(c);
          if (CurrentInputMode == InputMode.Password)
            monitor.Add('*');
          break;
        }
      }
      if (Input.GetKeyDown(KeyCode.Return) && programRunning == false && programRunningLast == false) {
        monitor.Add('\n');
        ExecuteInputBuffer();
        beganExecuting = true;
      }
    }
    monitor.EndDraw();
    if (!beganExecuting)
      programRunningLast = programRunning;
  }
  
  private bool CharIsTypeable(char ch) {
    return ch >= ' ' && ch <= '~';
  }

  private void SetupDefaultFileSystem() {
    var bin = FileSystem.Add(new Directory("bin")) as Directory;

    #region dir
    bin.Add(new Binary(
      "dir",
      "Displays a list of files and subdirectories in a directory.",
      null,
      dir
    ));
    #endregion
    #region clear
    bin.Add(new Binary(
      "clear",
      "Clears the screen.",
      null,
      clear
    ));
    #endregion
    #region cd
    bin.Add(new Binary(
      "cd",
      "Changes the current directory.",
      new Argument[] {
        new Argument("path", "path to change to")
      },
      "TODO: Write about how paths work here.",
      cd
    ));
    #endregion
    #region whoami
    bin.Add(new Binary(
      "whoami",
      "TODO",
      null,
      whoami)
    );
    #endregion
    #region help
    bin.Add(new Binary(
      "help",
      "Lists available commands and provides help information for executable binaries.",
      new Argument[] {
        new Argument("command", "command or binary file to get help for", false)
      },
      "If no command is provided, a list of available commands will be listed instead.",
      help
    ));
    #endregion
    #region login
    bin.Add(new Binary(
      "login",
      "TODO",
      new Argument[] {
        new Argument("username", "name of user to log in as", false),
        new Argument("password", "password of user")
      },
      login
    ));
    #endregion
    
    var dev = FileSystem.Add(new Directory("dev")) as Directory;
    var home = FileSystem.Add(new Directory("home")) as Directory;
    var usr = FileSystem.Add(new Directory("usr")) as Directory;
    var usrBin = usr.Add(new Directory("bin")) as Directory;
    foreach (User user in Users) {
      home.Add(new Directory(user.Name));
      usr.Add(new Directory(user.Name));
    }
    #region echo
    usrBin.Add(new Binary(
      "echo",
      "TODO",
      new Argument[] {
        new Argument("message", "Message to output", false, true)
      },
      echo
    ));
    #endregion
    #region users
    usrBin.Add(new Binary(
      "users",
      "TODO",
      null,
      users
    ));
    #endregion
    #region dump
    usrBin.Add(new Binary(
      "dump",
      "TODO",
      null,
      dump
    ));
    #endregion
  }
  public int ExecuteInputBuffer() {
    programRunning = true;
    programRunningLast = true;
    //string[] argv = InputBuffer.Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
    var argv = InputBuffer.Split('"')
      .Select((element, index) => index % 2 == 0  // If even index
        ? element.Split(new[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries)  // Split the item
        : new string[] { element })  // Keep the entire item
      .SelectMany(element => element).ToList();
    InputBuffer = "";
    
    if (argv.Count == 0) {
      programRunning = false;
      Done();
      return 0;
    }
    
    Binary command = GetCommand(argv[0]);
    if (command != null)
      return ExecuteBinary(command, argv);
    
    // We couldn't find it.
    UnknownCommandOrFileError(argv[0]);
    programRunning = false;
    Done();
    return -1;
  }
  
  public List<Binary> Commands {
    get {
      // TODO: Restrict based on user
      
      // /bin/
      var bins = FileSystem["bin"].Contents.Where(x => x is Binary).ToList();
      // /usr/bin/
      var usrBinBins = FileSystem["usr"]["bin"].Contents.Where(x => x is Binary);
      if (usrBinBins != null)
        bins.AddRange(usrBinBins);
      // /usr/[user]/
      var usrUserBins = FileSystem["usr"][CurrentUser.Name].Contents.Where(x => x is Binary);
      if (usrUserBins != null)
        bins.AddRange(usrUserBins);
      return bins.Cast<Binary>().ToList();
    }
  }
  public Binary GetCommand(string command) {
    // TODO: Restrict based on user
    
    // [current directory]
    File cwd = CurrentDirectory.Contents.Where(x => x.Name == command).FirstOrDefault();
    if (cwd != null) {
      Binary cwdAsBinary = cwd as Binary;
      if (cwdAsBinary != null)
        return cwdAsBinary;
    }
    
    // /bin/
    File bin = FileSystem["bin"].Contents.Where(x => x.Name == command).FirstOrDefault();
    if (bin != null) {
      Binary binAsBinary = bin as Binary;
      if (binAsBinary != null)
        return binAsBinary;
    }
    
    // /usr/bin/
    File usrBin = FileSystem["usr"]["bin"].Contents.Where(x => x.Name == command).FirstOrDefault();
    if (usrBin != null) {
      Binary usrBinAsBinary = usrBin as Binary;
      if (usrBinAsBinary != null)
        return usrBinAsBinary;
    }
    
    // /usr/[user]/
    File usrUser = FileSystem["usr"][CurrentUser.Name].Contents.Where(x => x.Name == command).FirstOrDefault();
    if (usrUser != null) {
      Binary usrUserAsBinary = usrUser as Binary;
      if (usrUserAsBinary != null)
        return usrUserAsBinary;
    }
    return null;
  }
  
  public int ExecuteBinary(Binary binary, List<string> argv) {
    if (binary.ArgumentsAreValid(argv)) {
      CurrentInputMode = InputMode.Suppressed;
      StartCoroutine(binary.Contents(argv));
      return 0;
    }
    ArgumentCountError();

    Done();
    return -1;
  }

  public int CursorLeft {
    get {
      return monitor.Cursor.X;
    }
  }
  public int CursorTop {
    get {
      return monitor.Cursor.Y;
    }
  }
  public void SetCursorPosition(int left, int top) {
    monitor.Cursor.X = left;
    monitor.Cursor.Y = top;
  }
  
  public void WritePrompt() {
    //Console.ForegroundColor = ConsoleColor.DarkGray;
    Write(CurrentDirectory.Path + ">");
    //Console.ForegroundColor = ConsoleColor.Gray;
  }
  
  public void Write(string value) {
    OutputBuffer += value;
    /*foreach (char c in value) {
      monitor.PlaySound_RecieveCharacter();
      monitor.Add(c);
    }*/
  }
  public void WriteLine(string value) {
    Write(value + "\n");
  }
  public void WriteLine() {
    Write("\n");
  }
  public void Clear() {
    monitor.Clear();
  }

  public void Done() {
    programRunning = false;
    CurrentInputMode = InputMode.Normal;
    WritePrompt();
  }
  
  public void ResetColor() {
    //Console.ResetColor();
  }
  
#region Executables
  IEnumerator dump(List<string> argv) {
    string toWrite = "";
    for (int j = 0; j < monitor.Height / 2; ++j) {
      int w = Random.Range(1, monitor.Width / 2);
      for (int i = 0; i < w; ++i) {
        if (Random.Range(0, 3) == 1)
          toWrite += " ";
        else
          toWrite += char.ConvertFromUtf32(Random.Range(33, 126));
      }
      toWrite += "\n";
    }
    Write(toWrite);
    WriteLine();
    Done();
    yield break;
  }
  IEnumerator echo(List<string> argv) {
    for (int i = 1; i < argv.Count; ++i)
      Write(argv[i] + (i == argv.Count - 1 ? "\n\n" : " "));
    Done();
    yield break;
  }
  IEnumerator dir(List<string> argv) {
    if (CurrentDirectory == FileSystem) {
      WriteLine(" Volume in drive is OS\n Volume serial number is 0000-0000-0000-0000\n");
      WriteLine(" " + CurrentDirectory.Path);
    }
    foreach (File f in CurrentDirectory.Contents.Where(x => x is Directory))
      WriteLine("  " + f.Name + "/");
    foreach (File f in CurrentDirectory.Contents.Where(x => !(x is Directory)))
      WriteLine("  " + f.Name);
    WriteLine();
    Done();
    yield break;
  }
  
  IEnumerator clear(List<string> argv) {
    Clear();
    Done();
    yield break;
  }
  IEnumerator cd(List<string> argv) {
    // TODO: Actual path handling
    if (argv[1] == "..") {
      if (CurrentDirectory.Parent != null)
        CurrentDirectory = CurrentDirectory.Parent;
      Done();
      yield break;
    }
    File f = CurrentDirectory.Contents.Where(x => x.Name == argv[1]).FirstOrDefault();
    if (f == null) {
      FileNotFoundError(argv[1]);
      Done();
      yield break;
    }
    Directory d = f as Directory;
    if (d == null) {
      FileNotDirectoryError(argv[1]);
      Done();
      yield break;
    }
    CurrentDirectory = d;
    Done();
    yield break;
  }
  IEnumerator help(List<string> argv) {
    if (argv.Count == 1) {
      WriteLine("For more information on a specific command, type\n  help [command-name]\n");
      
      foreach (Binary binary in Commands)
        WriteLine(" " + binary.Name);
      WriteLine();
    }
    
    Done();
    yield break;
  }
  IEnumerator whoami(List<string> argv) {
    WriteLine(CurrentUser.Name + "\n");
    Done();
    yield break;
  }

  IEnumerator login(List<string> argv) {
    if (argv[1] == CurrentUser.Name) {
      Error("Already logged in as \"" + argv[1] + "\".");
      Done();
      yield break;
    }
    
    User user = Users.Where(x => x.Name == argv[1]).FirstOrDefault();
    if (user == null) {
      Error("User \"" + argv[1] + "\" not found.");
      Done();
      yield break;
    }
    string password = argv.Count == 3 ? argv[2] : "";
    if (password == "") {
      Write("  Password: ");
      CurrentInputMode = InputMode.Password;
      do {
        yield return null;
      }
      while (!Input.GetKeyDown(KeyCode.Return));
      CurrentInputMode = InputMode.Suppressed;
      argv.Add(InputBuffer);
      InputBuffer = "";
      WriteLine();
    }
    
    if (argv[2] == user.Password) {
      CurrentUser = user;
      WriteLine("Logged in as \"" + argv[1] + "\" successfully.\n");
    }
    else
      WriteLine("Incorect password for user \"" + argv[1] + "\".\n");
    Done();
    yield break;
  }

  IEnumerator users(List<string> argv) {
    foreach (User user in Users)
      WriteLine(user.Name);
    WriteLine();
    Done();
    yield break;
  }
  #endregion
  
#region Errors
private void Error(string message) {
  //Console.BackgroundColor = ConsoleColor.DarkRed;
    //Console.ForegroundColor = ConsoleColor.Red;
    Write("ERROR:");
    //Console.BackgroundColor = ConsoleColor.Black;
    //Console.ForegroundColor = ConsoleColor.Gray;
    WriteLine(" " + message + "\n");
    
  }
  private void ArgumentCountError() {
    Error("Wrong number of arguments.");
  }
  private void ArgumentCountError(int expected) {
    Error("Wrong number of arguments (expected " + expected.ToString() + ").");
  }
  private void ArgumentCountError(int expected, int expected2) {
    Error("Wrong number of arguments (expected " + expected.ToString() + " or " + expected2.ToString() +  ").");
  }
  private void FileNotFoundError(string file) {
    Error("File \"" + file + "\" not found.");
  }
  private void FileNotDirectoryError(string file) {
    Error("File \"" + file + "\" is not a directory.");
  }
  private void UnknownCommandOrFileError(string file) {
    Error("Unknown command or binary filename: \"" + file + "\".");
  }
  #endregion
}